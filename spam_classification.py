### Classify spam with NB ###

from naive_bayes import *
import numpy as np
import random as random

def textParse(lots_of_text):
    import re
    # split the text on anything that isn't a word or number
    words = re.split(r'\W*', lots_of_text)
    # remove spaces and make everything LC
    return [word.lower() for word in words if len(word) > 2]

def catchSPAM():
    documents = []
    labels = []
    full_text = []
    for i in range(1, 26):

        # positive examples for spam
        wordlist = textParse(open('email/spam/%d.txt' % i).read())
        documents.append(wordlist)
        full_text.append(wordlist)
        labels.append(1)

        #negative examples for spam
        wordlist= textParse(open('email/ham/%d.txt' % i).read())
        documents.append(wordlist)
        full_text.append(wordlist)
        labels.append(0)

    vocab = createVocab(documents)
    trainingSet = range(50)
    testSet = []
    
    #Pick 10 random examples from training set and set them exclusively aside for testing
    for i in range(10):
        randIndex = int(random.uniform(0, len(trainingSet)))
        testSet.append(trainingSet[randIndex])
        del(trainingSet[randIndex])
    
    #Train
    trainMat = []
    trainCategory = []
    for document_index in trainingSet:
        trainMat.append(words2vector(vocab, documents[document_index]))
        trainCategory.append(labels[document_index])
    p0v, p1v, pSPAM = NB_classifier_train(np.array(trainMat), np.array(trainCategory))
    
    #Test
    errorCount =0
    for document_index in testSet:
        wordVector = words2vector(vocab, documents[document_index])
        if NB_classify(np.array(wordVector), p0v, p1v, pSPAM) != labels[document_index]:
            errorCount += 1
    
    print 'the error rate of the NB SPAM classifier is ', float(errorCount)/len(testSet)


    
