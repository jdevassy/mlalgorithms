from math import log
import operator

def createDataSet():
    """
    Create  toy dataset
    """
    dataset = [ [1, 1, 'yes'],
                [1, 1, 'yes'],
                [1, 0, 'no'],
                [0, 1, 'no'],
                [0, 1, 'no'] ]
    labels = ['no surfacing', 'flippers']
    return dataset, labels

def ShannonEntropy(dataset):
    """
    Calculate the Shannon Entropy of Dataset
     sum -(p(x)*log(p(x)))
    Amount of disorder
    More messy data -> higher entropy
    """
    numEntries = len(dataset)
    labelCounts = {}
    for each_feature_vector in dataset:
        currentLabel = each_feature_vector[-1] # last col
        if currentLabel not in labelCounts.keys():
            labelCounts[currentLabel] = 0
        labelCounts[currentLabel] += 1
    shannonEnt = 0.0
    for key in labelCounts:
        prob = float(labelCounts[key])/numEntries
        shannonEnt -= prob * log(prob, 2)
    return shannonEnt

def splitDataSet(dataset, axis, value):
    """
    Take in dataset to split, the feature we split on and the value of feature to return
    """
    retDataSet = [] #new list
    for each_feature_vector in dataset:
        if each_feature_vector[axis] == value:
            reducedFeatureVector = each_feature_vector[:axis] 
            reducedFeatureVector.extend(each_feature_vector[axis+1:])
            retDataSet.append(reducedFeatureVector)
    return retDataSet

def chooseBestFeature(dataset):
    """
    Choose best feature to split dataset on
    """
    numFeatures = len(dataset[0])-1
    baseEntropy = ShannonEntropy(dataset)
    bestInfoGain = 0.0
    bestFeature = -1
    
    for i in range(numFeatures):
        featureList = [example[i] for example in dataset]
        
        #create a unique list
        uniqueVals = set(featureList)
        newEntropy = 0.0
        
        #calculate entropy for each split
        for value in uniqueVals:
            subDataSet = splitDataSet(dataset, i, value)
            prob = len(subDataSet)/float(len(dataset))
            newEntropy += prob * ShannonEntropy(subDataSet)
        infoGain = baseEntropy - newEntropy
        
        #find the best gain
        if infoGain > bestInfoGain :
            bestInfoGain = infoGain
            bestFeature = i
    return bestFeature

def majorityVote(labels):
    """
    Majority voting as in KNN
    """
    classCount = {}
    for vote in labels:
        if vote not in classCount.keys():
            classCount[vote] = 0
        classCount[vote] += 1
        sortedclassCount = sorted(classCount, reverse=True)
        return sortedclassCount[0]
            

def createTree(dataset, labels):
    """
    Create a tree recursively
    """
    classlabels = [example[-1] for example in dataset]
    #return if all classes are labelled the same
    if classlabels.count(classlabels[0]) == len(classlabels):
        return classlabels[0] 
    # when no more feature, return majority
    if len(dataset[0]) == 1:
        return majorityVote(classlabels)
    bestFeature = chooseBestFeature(dataset)
    bestFeatureLabel = labels[bestFeature]
    decisiontree = {bestFeatureLabel : {} }
    subLabels = labels[:]
    del(subLabels[bestFeature])
    featureValues = [example[bestFeature] for example in dataset]
    uniqueVals = set(featureValues)
    for value in uniqueVals:
        #subLabels = labels[:]
        decisiontree[bestFeatureLabel][value] = createTree(splitDataSet(dataset, bestFeature, value), subLabels)
    return decisiontree

def classify(decisionTree, featureLabels, testVector):
    """
    Use tree to classify a new datapoint 
    """
    #find first feature the tree splits on
    firstStr = decisionTree.keys()[0]
    secondDict = decisionTree[firstStr]
    #Use index() to find first element in featureLabels that matches firstStr
    featureIndex = featureLabels.index(firstStr)
    for key in secondDict.keys():
        if testVector[featureIndex] == key:
            #do we further go down?
            if type(secondDict[key]).__name__=='dict':
                classLabel= classify(secondDict[key], featureLabels, testVector)
            #have we reached a leaf?
            else:
                classLabel = secondDict[key]
    return classLabel

def storeTree(tree, filename):
    """
    Building the tree each time is majority of work repeated unnecessarily
    Store tree so that we can do classification quickly without tree construction
    Persistent tree by serialization
    Unlike KNN, trees can be distilled and kept in memory to provide info for future examples
    """
    import pickle
    f = open(filename, 'w')
    pickle.dump(tree, f)
    f.close()

def getTree(filename):
    """
    fetch the tree from pickle dump
    """
    import pickle
    f = open(filename)
    return pickle.load(f)


#test#
# data, labels = createDataSet()
# tree = createTree(data, labels)
# classify(tree, labels, [1, 0])
# classify(tree, labels, [1, 1])
###

###Test on UCI Lenses dataset###
f = open('lenses.txt')
lenses = [line.strip().split('\t') for line in f.readlines()]
labels = ['age', 'prescript', 'astigmatic', 'tearRate']
lenses_tree = createTree(lenses, labels)

### ID3 - overfits on data. Redo with CART
