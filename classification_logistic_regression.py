"""
Classification using logistic regression

Text preprocessing - replace missing values with 0 - for logistic regression thworks because sigmoid(0) = 1/2. Also weights = weights + alpha*error*data[i] and if data[i] = 0, then weights = weights in the update of weights at each iteration

Missing classlabel - throw this feature out

Dataset - UCI Datasets for Horses
"""
from logistic_regression import *
import numpy as np

def classify(input, weights):
    input = np.mat(input)
    weights = np.mat(weights)
    prob = sigmoid(sum(input*weights.transpose()))
    if prob > 0.5:
        return 1.0
    else:
        return 0.0

def test():
    training = open('trainingData.txt')
    testing = open('testingData.txt')
    trainingSet = []
    trainingLabels = []
    for line in training.readlines():
        line = line.strip().split('\t')
        lines = []
        for i in range(21):
            lines.append(float(line[i]))
        trainingSet.append(lines)
        trainingLabels.append(float(line[21]))
    trainingWeights = stochasticGradientAscent(np.array(trainingSet), trainingLabels, 500)
    errorCount = 0
    numTestVectors = 0
    for line in testing.readlines():
        numTestVectors += 1
        line = line.strip().split('\t')
        lines = []
        for i in range(21):
            lines.append(float(line[i]))
        if int(classify(np.array(lines), trainingWeights)) != int(line[21]):
            errorCount += 1
    errorRate = (float(errorCount)/numTestVectors)
    print "error rate of classifier: %f" % errorRate
    return errorRate

def multipleTests():
    numTests = 10
    errorSum = 0.0
    for k in range(numTests):
        errorSum += test()
    errorRate = errorSum/float(numTests)
    print "after %d test iterations, error rate: %f" % (numTests, errorRate)
    return errorRate

multipleTests()

