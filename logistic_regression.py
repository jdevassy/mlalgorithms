import numpy as np
import random as random

def loadDataSet(filename):
    """
    Create a toy dataset
    """
    data = []
    labels = []
    f = open(filename, 'r')
    for line in f.readlines():
        line = line.strip().split()
        # each line has a x-y point and a class label. Add a weight column to data and set it to
        data.append([1.0, float(line[0]), float(line[1])])
        labels.append(int(line[2]))
    return data, labels

def sigmoid(input):
    """
    sigmoid regression function for logistic regression - flatten to 0-1range
    """
    return 1.0/(1+np.exp(-input))


def gradientAscent(data, labels):
    data = np.mat(data)
    labels = np.mat(labels)
    m, n = np.shape(data)
    alpha = 0.001 # magnitude of growth at each step
    maxSteps = 500
    weights = np.ones((n, 1))
    for step in range(maxSteps):
        h = sigmoid(data*weights)
        error = (labels-h)
        weights = weights + alpha * data.transpose() * error
    return weights

def stochasticGradientAscent(data, labels, numIterations = 100):
    data = np.mat(data)
    m, n = np.shape(data)
    weights = np.ones(n)
    weights = np.mat(weights)
    for j in range(numIterations):
        dataIndex = range(m)
        for i in range(m):
            #update alpha during each iteration to reduce HF oscillations
            alpha = 4/(1.0+j+i) + 0.01
            #reduce periodic vibrations by randomly choosing the update index
            randIndex = int(random.uniform(0, len(dataIndex)))
            h = sigmoid(sum(data[randIndex]*weights.transpose()))
            error = labels[randIndex] - h
            weights = weights + alpha * error * data[randIndex]
            del (dataIndex[randIndex])
    return weights

def plot(weights):
    """
    Plot the decision boundary
    """
    import matplotlib.pyplot as plt
    data, labels = loadDataSet('testSet.txt')
    data = np.array(data)
    n = np.shape(data)[0]
    weights = np.array(weights).reshape(-1,).tolist()
    xcoord1 = []
    ycoord1 = []
    xcoord2 = []
    ycoord2 = []
    for i in range(n):
        if int(labels[i]) == 1:
            xcoord1.append(data[i,1])
            ycoord1.append(data[i,2])
        else:
            xcoord2.append(data[i,1])
            ycoord2.append(data[i,2])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(xcoord1, ycoord1, s=30, c='red', marker='s')
    ax.scatter(xcoord2, ycoord2, s=30, c='green')
    x = np.arange(-3.0, 3.0, 0.1)
    y = -weights[0]-weights[1]*x/weights[2]
    ax.plot(x,y)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.show()

data, labels = loadDataSet("testSet.txt")
#weights = gradientAscent(data, labels)
weights = stochasticGradientAscent(data, labels)
print weights
plot(weights)
