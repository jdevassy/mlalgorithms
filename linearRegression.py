import numpy as np

def loadDataSet(filename):
    numFeatures = len(open(filename).readline().split('\t'))
    data = []
    labels = []
    f = open(filename)
    for line in f.readlines():
        lines = []
        line = line.strip().split('\t')
        for i in range(numFeatures):
            lines.append(float(line[i]))
        data.append(lines)
        labels.append(float(line[-1]))
    return data, labels

def linearRegression(x, y):
    x = np.mat(x)
    xTx = x.T*x
    y = np.mat(y).T
    if np.linalg.det(xTx) == 0.0:
        print "Singular Matrix not invertible "
        return
    w = xTx.I * (x.T*y)
    return w

def locally_weighted_linear_regression(x, y, k=1.0, input):
    x = np.mat(x)
    y = np.mat(y).T
    m = np.shape(x)[0]
    weights = np.mat(eye(m))
    for j in range(m):
        diff = input - x[j,:]
        weights[j,j] = np.exp(diff*diff.T/(-2.0*k**2)) # exponentially decaying weights
    xTx = x.T*(weights*x)
    if linalg.det(xTx) == 0.0:
        print "Singular Matrix"
        return 
    w = xTx.I * (x.T * (weights * y))
    return input * w

def lwlr_test(testPoints, x, y, k=1.0):
    # k = kernel size for local weighting
    m = np.shape(testPoints)[0]
    yHat = np.zeros(m)
    for i in range(m):
        yHat[i] = locally_weighted_linear_regression(testPoints[i], x, y, k)
    return yHat

def lsq_error(y, yHat):
    return ((y - yHat) ** 2).sum()

def ridge_regression(x, y, lambda_ = 0.2):
    # when n>m
    # when features > data points
    # when data isn't full rank -> we get an error if we do inverse(xTx)
    # Ridge regression - add an additional lambda*I to the matrix xTx so that we can invert it
    #shrinkage = impose penalty on unimportant features
    xTx = x.T*x
    denom = xTx + np.eye(np.shape(x)[1])*lambda_
    if linalg.det(denom) == 0.0:
        print "Non-invertible singular"
        return
    w = denom.I * (x.T*y)
    return w

def ridgeTest(x,y):
    x = np.mat(x)
    y = np.mat(y).T
    y = y - np.mean(y,0)
    x = (x - np.mean(x,0))/np.var(x, 0)
    numTestPoints = 30
    w = np.zeros((numTestPoints, np.shape(x)[1]))
    for i in range(numTestPoints):
        weight = ridgeRegression(x, y, exp(i-10))
        w[i,:] = weight.T
    return w

x, y = loadDataSet('ex0.txt')
w = linearRegression(x,y)
print w
ridgeweights = ridgeTest(x,y)
print ridgeweights

