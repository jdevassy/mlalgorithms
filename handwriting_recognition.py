### A KNN Classifier for HandWriting Recognition ###

from os import listdir
import numpy as np
from knn import *

def img_to_Vector(filename):
    """
    Convert the 2D Images to a 1D Vector
    32 x 32 images -> 1 x 1024 vector
    
    """
    vec = np.zeros( (1, 1024) ) 
    f = open(filename)
    for i in range(32):
        line = f.readline()
        for j in range(32):
            vec[0, 32*i+j] = int(line[j])
    return vec

def handwritingClassTest():
    labels = []
    trainingFileList = listdir('trainingDigits')
    M = len(trainingFileList)
    trainingMat = np.zeros((M, 1024))
    for i in range(M):
        fileName = trainingFileList[i]
        fileStr = fileName.split('.')[0]
        classNumStr = int(fileStr.split('_')[0])
        labels.append(classNumStr)
        trainingMat[i,:] = img_to_Vector('trainingDigits/%s' % fileName)
    testFileList = listdir('testDigits')
    errorCount = 0.0
    for i in range(len(testFileList)):
        fileName = testFileList[i]
        fileStr = fileName.split('.')[0]
        classNumStr = int(fileStr.split('_')[0])
        current = img_to_Vector('testDigits/%s' % fileName)
        classifierResult = knn_classify(current, trainingMat, labels, 3)
        print "Classifier gives %s, the real answer %s" % (classifierResult, classNumStr)
        if classifierResult != classNumStr:
            errorCount += 1
    print "Error Count %d, Error Rate %f" % (errorCount, (errorCount/float(len(testFileList)))) 
