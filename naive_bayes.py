# Naive Bayes Classifier #

import numpy as np

def loadDataSet():
    """
    Toy DataSet
    """
    words = [ ['my', 'dog', 'has', 'flea', 'problems', 'help', 'him', 'please'], ['maybe', 'not', 'take', 'him', 'to', 'floozies'], ['stop', 'your', 'garbage'], ['my', 'lice', 'are', 'on', 'stupid'], ['stop', 'your', 'stupid'], ['cute', 'dog'], ['help', 'the', 'dogs' ]]
    classVec =[0, 1, 1, 1, 1, 0, 0]
    return words, classVec

def createVocab(dataset):
    """
    Create a vocabulary (return set for unique)
    """
    vocab = set([])
    #create a union over sets
    for document in dataset:
        vocab = vocab | set(document)
    return list(vocab)

def words2vector(vocab, input):
    """
    BAG OF WORDS
    return a vector of 1s and 0s
    if input doc has a word present in vocab, set that v[vocab_index of word] = 1
    """
    #create a vector of all zeros
    returnVec = [0] * len(vocab)
    for word in input:
        if word in vocab:
            returnVec[vocab.index(word)] += 1
        else:
            print "word %s not in vocabulary" % word
    return returnVec

def NB_classifier_train(trainMatrix, trainCategory):
    """
    trainMatrix = input documents as a matrix
    trainCategory = vector of class labels for each document
    """
    num_documents = len(trainMatrix)
    num_words = len(trainMatrix[0])

    #2-class problem - so sum the 1's and divide by total num of documents
    pAbusive = sum(trainCategory)/float(num_documents)
  
    # we can't do np.zeros(num_words) because we'll multiply under naive bayes independence and get 0s!
    p0Num = np.ones(num_words)
    p1Num = np.ones(num_words)
    #similarly for denom
    p0Denom = 2.0
    p1Denom = 2.0
    
    for i in range(num_documents):
        if trainCategory[i] == 1:
            p1Num += trainMatrix[i]
            p1Denom += sum(trainMatrix[i])
        else:
            p0Num += trainMatrix[i]
            p0Denom += sum(trainMatrix[i])
            
    # log calculations to prevent underflow in division
    p1Vect = np.log(p1Num/p1Denom)
    p0Vect = np.log(p0Num/p0Denom)
    
    return p0Vect, p1Vect, pAbusive

def NB_classify(input, p0vec, p1vec, pAbusive):
    """
    Binary classification with NB on new input  
    """
    p1 = sum(input*p1vec) + np.log(pAbusive)
    p0 = sum(input*p0vec) + np.log(1.0 - pAbusive)
    if p1 > p0:
        return 1
    else:
        return 0

def NB_test():
    
    #Load the dataset and make a vocabulary of unique words
    data, labels = loadDataSet()
    vocab = createVocab(data)
   
    #Train the Classifier
    trainMat = []
    for item in data:
        trainMat.append(words2vector(vocab, item) )
        p0v, p1v, pabusive = NB_classifier_train(np.array(trainMat), np.array(labels))
       
    #Test Input 1
    testInput = ['love', 'my', 'dog']
    testDoc = np.array(words2vector(vocab, testInput))
    print testInput, 'classified ', NB_classify(testDoc, p0v, p1v, pabusive)

    #Test Input 2
    testInput = ['stupid', 'dog']
    testDoc = np.array(words2vector(vocab, testInput))
    print testInput, 'classified ', NB_classify(testDoc,p0v, p1v, pabusive)


### test ###
NB_test()
