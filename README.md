# Prototyping a few ML algorithms in Python

1. A 3NN classifier
2. A classifier for Handwriting Recognition (3NN)
3. Decision Tree for Classification (ID3)
4. Naive Bayes with Bag of Words Model for Document Classification
5. Spam Classifier for emails with Naive Bayes
6. Finding the local flavour on Craiglist RSS feeds using Naive Bayes
7. Logistic Regression with Gradient Methods 
8. Logistic Regression for Classification 
9. Platt's Sequential Minimal Optimization Routine for fixing b and Alphas for SVMs 
10.Adaboosting for Classification
11. Linear Regression
