# Take a bunch of RSS feeds from a local area and see what words might be common to that locality 
# CRAIGSLIST!

from naive_bayes import *
from spam_classification import textParse
import numpy as np
import random as random
from feedparser import *

def calcMostFreq(vocab, lots_of_text):
    """
    Return the top 50 words
    """
    import operator
    mostFreq = {}
    for token in vocab:
        mostFreq[token] = lots_of_text.count(token)
    sortedFreq = sorted(mostFreq, reverse=True)
    return sortedFreq[:50]

def localWords(feed1, feed0):
    """
    Look into the local flavour on Craiglist
    """
    documents = []
    labels = []
    lots_of_text = []
    minLength = min(len(feed1['entries']),len(feed0['entries']))
    for i in range(minLength):

        # positive examples to one location
        wordList = textParse( feed1['entries'][i]['summary']) 
        documents.append(wordList)
        lots_of_text.extend(wordList)
        labels.append(1)

        #negative examples from another location
        wordList = textParse( feed0['entries'][i]['summary'])
        documents.append(wordList)
        lots_of_text.extend(wordList)
        labels.append(0)
        
    vocab = createVocab(documents)
    top50Words = calcMostFreq(vocab, lots_of_text)
    for pairW in top50Words:
        if pairW[0] in vocab:
            vocab.remove(pairW[0])

    trainingSet = range(2*minLength)
    testSet = []

    #Make a Test Set by picking randomly from Training Set and setting aside those examples
    for i in range(20):
        randIndex = int(random.uniform(0, len(trainingSet)))
        testSet.append(trainingSet[randIndex])
        del(trainingSet[randIndex])

    trainMat = []
    trainCategory = []
    
    # Train
    for document_index in trainingSet:
        trainMat.append(words2vector(vocab, documents[document_index]) )
        trainCategory.append(labels[document_index])
    p0v, p1v, pSpam = NB_classifier_train(np.array(trainMat), np.array(trainCategory))
    
    errorCount = 0
    #Test
    for document_index in testSet:
        wordVector = words2vector(vocab, documents[document_index])
        if NB_classify(np.array(wordVector), p0v, p1v, pSpam) != labels[document_index]:
            errorCount += 1
    print 'error rate :  ', float(errorCount)/len(testSet)
    return vocab, p0v, p1v
     

def getTopWords(atl, bay):
    import operator
    vocab, p_bay, p_atl = localWords(atl, bay)
    topATL = []
    topBAY = []
    for i in range(len(p_bay)):
        if p_bay[i] > -6.0 :
            topBAY.append( (vocab[i], p_bay[i] ))
        if p_atl[i] > -6.0 :
            topATL.append( (vocab[i], p_atl[i] ))
    sortedBAY = sorted(topBAY, reverse=True)
    sortedATL = sorted(topATL, reverse=True)
    print 'Top words of the local flavour on Craiglist ATL '
    for item in sortedATL:
        print item[0]
    print 'Top words of the local flavour on Craiglist BAY AREA '
    for item in sortedBAY:
        print item[0]
    return

atl = parse('http://atlanta.craigslist.org/stp/index.rss')
bay = parse('http://sfbay.craigslist.org/stp/index.rss')
getTopWords(atl, bay)

