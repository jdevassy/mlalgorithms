### KNN Algorithm ###

import numpy as np
import operator #for sorting

def file_to_Matrix(filename):
    """Take a txt file of data and parse it into a numpy datamatrix and a vector of class labels"""
    f = open(filename, 'r')
    num_lines = len(f.readlines()) # count num lines to allocate np matrix
    M = np.zeros((num_lines, 3))
    classLabel = [] # a vector of class labels
    f = open(filename, 'r')
    index = 0
    for line in f.readlines():
        line = line.strip().split('\t')
        M[index, :] = line[0:3]
        classLabel.append(line[-1])
        index += 1
    return M, classLabel
    
def createDataSet():
    """
    Create a toy dataset
    """
    group = np.array([[1.0, 1.1], [1.0, 1.0], [0.0, 0.0], [0.0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels

def autoNorm(dataSet):
    """Normalize
    normalized_value = (old_value - min)/(max-min)
    """
    minVals = dataSet.min(0)
    maxVals = dataSet.max(0)
    ranges = maxVals - minVals
    normalizedSet = np.zeros(np.shape(dataSet))
    M = dataSet.shape[0]
    normalizedSet = dataSet - np.tile(minVals, (M, 1) )
    normalizedSet = normalizedSet/np.tile(ranges, (M, 1) )
    return normalizedSet, ranges, minVals

def knn_classify(input_x, dataSet, labels, k):
    """
    input_x = new data to be classified
    dataSet = existing dataset
    labels = classified labels for dataset
    k = number of NN to consider
    """
    dataSetSize = dataSet.shape[0]
    
    ###calculate the Euclidean distance between input_x and each point in the dataset
    diffMat = np.tile(input_x, (dataSetSize, 1)) - dataSet
    sqDiffMat = diffMat ** 2
    sqDistances = sqDiffMat.sum(axis=1)
    distances = sqDistances ** 0.5
    
    ### Sort the distances in increasing order
    sortedDistIndices = distances.argsort()
    
    ###take k neighbours with lowest distances to input_X
    classCount = {}
    for i in range(k):
        votelabel = labels[sortedDistIndices[i]]
        classCount[votelabel] = classCount.get(votelabel, 0) + 1

    ###Sort and get the label 
    sortedClassCount = sorted(classCount, reverse=True)
    return sortedClassCount[0]

def Test():
    """Test the KNN classifier and find accuracy"""
    hoRatio = 0.10
    datingMatrix, Labels = file_to_Matrix('datingTestSet.txt')
    normMat, ranges, minVals = autoNorm(datingMatrix) ###Normalize
    M = normMat.shape[0]
    num_test_vectors = int(M*hoRatio)
    errorCount = 0.0
    for i in range(num_test_vectors):
        classifierResult = knn_classify(normMat[i,:], normMat[num_test_vectors:M,:], Labels[num_test_vectors:M], 3)
        print "Classifier: %s, True Result: %s" % (classifierResult, Labels[i])
        if classifierResult != Labels[i]:
            errorCount += 1
    print "Error Rate: %f" % (errorCount/float(num_test_vectors))


### Classify a new data point ###
def classifyNewPerson():
    resultList = ['didntLike', 'smallDoses', 'largeDoses']
    attrib_1 = float(raw_input("attribute 1  " ))
    attrib_2 = float(raw_input("attribute 2  " ))
    attrib_3 = float(raw_input("attribute 3  " ))
    DataMatrix, DataLabels = file_to_Matrix('datingTestSet.txt')
    normMat, ranges, minVals = autoNorm(DataMatrix)
    input_x = np.array( [attrib_1, attrib_2, attrib_3] )
    classifierResult = knn_classify( (input_x - minVals)/ranges, normMat, DataLabels, 3)
    print "Result: ", classifierResult

###Accuracy of KNN Classifier###
Test() 
###Classify new Data ###
classifyNewPerson()
