# Platt's Sequential Minimal Optimization algorithm to train SVM #

import random as random
import numpy as np

def loadDataSet(filename):
    data =[]
    labels = []
    f = open(filename, 'r')
    for line in f.readlines():
        lines = line.strip().split('\t')
        data.append([float(lines[0]), float(lines[1])])
        labels.append(float(lines[2]))
    return data, labels

def selectJRand(first_alpha_index, total_num_alphas):
    j = first_alpha_index
    while (j==first_alpha_index):
        #randomly choose a value that is not the current value of alpha
        j = int(random.uniform(0, total_num_alphas))
    return j

def clip_alpha(alpha, higher, lower):
    if alpha > higher:
        alpha = higher
    if alpha < lower:
        alpha = lower
    return alpha

def svm_smo(data, labels, C, tolerance, maxIterations = 150):
    data = np.mat(data)
    labels = np.mat(labels).transpose()
    b = 0
    m, n = np.shape(data)
    alphas = np.mat(np.zeros((m, 1)))
    iteration = 0 
    while (iteration < maxIterations):
        alphaPairsChanged = 0
        for i in range(m):
            fXi = float(np.multiply(alphas, labels).T* 
                        (data*data[i,:].T)) + b
            Ei = fXi - float(labels[i])

            #Can alphas be changed? If so, enter optimization routine
            if((labels[i] * Ei < -tolerance) and (alphas[i] < C)) or ((labels[i] * Ei > tolerance) and (alphas[i] > 0)) :
                #Randomly select second alpha
                j = selectJRand(i, m)
                fXj = float(np.multiply(alphas, labels).T*
                            (data*data[j,:].T)) + b
                Ej = fXj - float(labels[j])
                alphaIold = alphas[i].copy()
                alphaJold = alphas[j].copy()
                #make sure that alphas are bounded between 0 and C
                if labels[i] != labels[j]:
                    lower = max(0, alphas[j] - alphas[i])
                    higher = min(C, C+alphas[j] - alphas[i])
                else:
                    lower = max(0, alphas[j] + alphas[i] - C)
                    higher = min(C, alphas[j] + alphas[i])
                if lower == higher:
                    print "lower == higher"
                    continue
                eta = 2.0 * data[i,:]*data[j,:].T - data[i,:]*data[i,:].T - data[j,:]*data[j,:].T
                if eta >= 0:
                    print "eta >=0 " 
                    continue
                alphas[j] = alphas[j] - labels[j]*(Ei-Ej)/eta
                alphas[j] = clip_alpha(alphas[j], higher, lower)
                if abs(alphas[j] - alphaJold) < 0.00001:
                    print "j updated too slow"
                    continue
                alphas[i] = alphas[i] + labels[j] * labels[j] * (alphaJold - alphas[j])
                b1 = b - Ei - labels[i] * (alphas[i] - alphaIold) * data[i,:] * data[i,:].T  - labels[j] * (alphas[j] - alphaJold) * data[i, :] * data[j, :].T
                b2 = b - Ei - labels[i]* (alphas[i] - alphaIold) * data[i,:] *\
                    data[j,:].T  -labels[j] * (alphas[j] - alphaJold) * data[j, :] * data[j, :].T
                if 0 < alphas[i] and C > alphas[i] :
                    b = b1
                if 0 < alphas[j] and C > alphas[j]:
                    b = b2
                else:
                    b = (b1+b2)/2.0
                alphaPairsChanged += 1
            if (alphaPairsChanged == 0):
                iteration += 1
            else:
                iteration = 0
    return b, alphas

data, labels = loadDataSet('testDataSet.txt')
b, alphas = svm_smo(data, labels, 0.6, 0.001, 40)
print b, alphas

