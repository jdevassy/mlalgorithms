import numpy as np

def loadData():
    """
    Toy dataset
    """
    data = np.matrix([ [1., 2.1], 
                    [2, 1.1], 
                    [1.3, 1.],
                    [1., 1.], 
                    [2., 1.] ])
    labels = [1.0, 1.0, -1.0, -1.0, 1.0]
    return data, labels

def stumpClassify(data, dimension, threshold, thresholdIneq):
    """
    Use a decision stump to classify on feature == dimension using condition thresholdIneq wrto threshold value
    """
    decision = np.ones((np.shape(data)[0], 1))
    if thresholdIneq == 'lt':
        decision[data[:, dimension] <= threshold] = -1.0
    else:
        decision[data[:, dimension] > threshold] = -1.0
    return decision

def buildDecisionStump(data, labels, weights):
    data = np.mat(data)
    m, n = np.shape(data)
    labels = np.mat(labels).T
    numSteps = 10
    bestStump = {}
    bestClassEst= np.mat(np.zeros((m, 1)))
    minError = np.inf
    for i in range(n):
        rangeMin = data[:, i].min()
        rangeMax = data[:, i].max()
        stepSize = (rangeMax-rangeMin)/numSteps
        for j in range(-1, int(numSteps)+1):
            for inequal in ['lt', 'gt']:
                threshold = (rangeMin + float(j) * stepSize)
                predictedVals = stumpClassify(data, i, threshold, inequal)
                errorCache = np.mat(np.ones((m, 1)))
                errorCache[predictedVals == labels] = 0
                weightedError = weights.T*errorCache
                print "split: dim %d, threshold %.2f, threshold condition %s, the weighted error is %.3f" % (i, threshold, inequal, weightedError)
                if weightedError < minError:
                    minError = weightedError
                    bestClassEst = predictedVals.copy()
                    bestStump['dim'] = i
                    bestStump['threshold'] = threshold
                    bestStump['inequal'] = inequal
    return bestStump, minError, bestClassEst

def adaBoostTraining(data, labels, numIterations = 40):
    weakClass = []
    m = np.shape(data)[0]
    weights = np.mat(np.ones((m,1)))
    aggClassEst = np.mat(np.zeros((m,1)))
    for i in range(numIterations):
        bestStump, minError, bestClassEst = buildDecisionStump(data, labels, weights)
        print "weights:", weights.T
        alpha = float(0.5*np.log((1.0-minError)/max(minError, 1e-16)))
        bestStump['alpha'] = alpha
        weakClass.append(bestStump)
        print "classEst:", bestClassEst.T
        expon = np.multiply(-1*alpha*np.mat(labels).T, bestClassEst)
        weights = np.multiply(weights, np.exp(expon))
        weights = weights/weights.sum()
        aggClassEst += alpha * bestClassEst
        print "aggclassEst:", aggClassEst.T
        aggError = np.multiply(np.sign(aggClassEst) != np.mat(labels).T, np.ones((m,1)))
        errorRate = aggError.sum()/m
        print "Error:", errorRate
        if errorRate == 0.0:
            break
    return weakClass
        
def adaBoostClassify(data, weakClass):
    data = np.mat(data)
    m = np.shape(data)[0]
    aggClassEst = np.mat(np.zeros((m,1)))
    for i in range(len(weakClass)):
        classEst = stumpClassify(data, weakClass[i]['dim'], 
                                 weakClass[i]['threshold'],
                                 weakClass[i]['inequal'])
        aggClassEst += weakClass[i]['alpha']*classEst
        print aggClassEst
    return np.sign(aggClassEst)

data, labels = loadData()
D = np.mat(np.ones((5, 1))/5)
buildDecisionStump(data, labels, D)
weakClass = adaBoostTraining(data, labels, 30)
adaBoostClassify([0, 0], weakClass)
